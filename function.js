function start() {
  let dateTime = new Date();
  let hours = dateTime.getHours();
  let minutes = dateTime.getMinutes();
  let seconds = dateTime.getSeconds();
  if (hours > 12) {
    hours = hours - 12;
  }
  document.getElementById("hTimes").innerHTML = hours;
  document.getElementById("mTimes").innerHTML = minutes;
  document.getElementById("sTimes").innerHTML = seconds;
  let myIcons = "<ion-icon name='stop-outline'></ion-icon>";
  document.getElementById("buttons").innerHTML = `${myIcons}Stop`;
  document
    .getElementById("buttons")
    .setAttribute(
      "class",
      "bg-red-500 h-10  w-1/4 mt-5 mb-5 font-bai1  text-white py-2 px-4 rounded"
    );

  document.getElementById("buttons").setAttribute("onclick", "stop()");
}

function stop() {
  let dateTime = new Date();
  let hours = dateTime.getHours();
  let minutes = dateTime.getMinutes();
  let seconds = dateTime.getSeconds();
  if (hours > 12) {
    hours = hours - 12;
  }
  document.getElementById("Stimes").innerHTML = hours;
  document.getElementById("Smines").innerHTML = minutes;
  document.getElementById("Ssec").innerHTML = seconds;
  let myIcons = "<ion-icon name='trash-bin-outline'></ion-icon>";
  document.getElementById("buttons").innerHTML = `${myIcons} Clear`;
  document
    .getElementById("buttons")
    .setAttribute(
      "class",
      "bg-yellow-500 h-10 font-bai1  w-1/4 mt-5 mb-5  text-white py-2 px-4 rounded"
    );
  document.getElementById("buttons").setAttribute("onclick", "Myclear()");
  totalTime();
}
function totalTime() {
  let StartTime = document.getElementById("StartTime").children[0].textContent;
  let StartMin = document.getElementById("StartTime").children[2].textContent;
  let Stoptime = document.getElementById("StopTime").children[0].textContent;
  let StopMin = document.getElementById("StopTime").children[2].textContent;

  let TotalTime = Stoptime - StartTime;
  console.log(TotalTime);
  if (TotalTime > 0) {
    TotalTime = TotalTime * 60;
  }

  let TotalMinutes = TotalTime + +(StopMin - StartMin);
  document.getElementById("Kitmin").innerHTML = `${TotalMinutes}`;

  Riel(TotalMinutes);
}
function Riel(mong) {
  let other = mong % 15;
  if (other > 0) {
    mong = mong - other;
    mong = (mong / 15) * 500 + 500;
  } else {
    mong = (mong / 15) * 500;
  }
  Kitluy.innerHTML = mong;
}

function Myclear() {
  document.getElementById("hTimes").innerHTML = "00";
  document.getElementById("mTimes").innerHTML = "00";
  document.getElementById("sTimes").innerHTML = "00";
  document.getElementById("Stimes").innerHTML = "00";
  document.getElementById("Smines").innerHTML = "00";
  document.getElementById("Ssec").innerHTML = "00";
  document.getElementById("Kitmin").innerHTML = "0";
  document.getElementById("Kitluy").innerHTML = "0";
  let myIcons = "<ion-icon name='play-outline'></ion-icon>";
  document.getElementById("buttons").innerHTML = `${myIcons} Start`;
  document
    .getElementById("buttons")
    .setAttribute(
      "class",
      "bg-pink-300 h-10 font-bai1  w-1/4 mt-5 mb-5  text-white py-2 px-4 rounded"
    );
  document.getElementById("buttons").setAttribute("onclick", "start()");
}

function myRunTime() {
  let today = new Date();

  let day = today.getDay();
  let month = today.getMonth();
  let year = today.getFullYear();
  let dayNum = today.getDate();
//   if (hours < 12) {
//     session.innerHTML = "AM";
//   } else {
//     session.innerHTML = "PM";
//   }

  let hours = today.getHours();
  let minutes = today.getMinutes();
  let seconds = today.getSeconds();
  let session = "AM";
  if (hours > 12) {
    hours = hours - 12;
    session = "PM";
  }

  let daylist = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday ",
    "Thursday",
    "Friday",
    "Saturday",
  ];
  let monthlist = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
  ];
  let mySet = [
    "Day",
    "Month",
    "DayNum",
    "Year",
    "Hour",
    "Minute",
    "Second",
    "session",
  ];
  let value = [
    daylist[day],
    monthlist[month],
    dayNum,
    year,
    hours,
    minutes,
    seconds,
    session,
  ];

  for (let i = 0; i < mySet.length; i++) {
    document.getElementById(mySet[i]).innerHTML = value[i];
  }
}
function myTime() {
  myRunTime();
  setInterval("myRunTime()", 1);
}
